/* Site Navigation */

//////////
// TODO: …
//////////

window.onload = function() { 
    const header = document.getElementById('header');
    const toggle = document.getElementById('toggle');
    const navbar = document.getElementById('navbar');
        
    // This function ...
    toggle.onclick = function() {
        toggle.classList.toggle('active');
        navbar.classList.toggle('active');
    }
    // This function ...
    document.onclick = function(e) {
        if (e.target.id !== 'header' && e.target.id !== 'toggle' && e.target.id !== 'navbar') {
            toggle.classList.remove('active');
            navbar.classList.remove('active');
        }
    }  
};


scrollTTBtn = document.getElementById('backTTBtn'); // TTB = To Top Button
window.onscroll = function() {showBTTBtn}; // BTT = Back To Top

function showBTTBtn() {
    if (document.body.scrollTop > 50 || document.documentElement.scrollTop > 50) {
        scrollTTBtn.style.display = "block";
    }
    else {
        scrollTTBtn.style.display = "none";
    }
}

function backToTopButton() {
    document.body.scrollTop = 0; // For Safari
    document.documentElement.scrollTop = 0; // For Chrome, Firefox, IE and Opera
}