/* SLIDESHOW in About Section */

/*window.onload = function() {
    // This line of code, clicks the first dot of the image slider in the about section, to load the image when the site is called or reloaded
    document.getElementById('clickDotToLoadImage').click();
};*/

var slideImageIndex = 1;
imageSlider(slideImageIndex);

// Next / Previous controls
function plusSlides(n) {
  imageSlider(slideImageIndex += n);
}

// Thumbnail image controls
function currentSlide(n) {
  imageSlider(slideImageIndex = n);
}

function imageSlider(n) {
  var i;
  var slides = document.getElementsByClassName("sliderImage");
  var dots = document.getElementsByClassName("imageSliderDot");
  
  if (n > slides.length) {slideImageIndex = 1}
  if (n < 1) {slideImageIndex = slides.length}

  for (i = 0; i < slides.length; i++) {
      slides[i].style.display = "none";
  }

  for (i = 0; i < dots.length; i++) {
      dots[i].className = dots[i].className.replace(" active", "");
  }

  slides[slideImageIndex-1].style.display = "block";
  dots[slideImageIndex-1].className += " active";
}