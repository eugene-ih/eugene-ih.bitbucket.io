/* Contact Formular */

//////////
// 1 - TODO: Make function, when the contact formular was successfully submitted that an green message pops up above the form
// 2 - TODO: Make function, that disables the contact-form submit button for 5 seconds, after it was clicked - contactForm.addEventListener()
//////////

const submitButton = document.getElementById('contactFormSubmit');
const contactForm = document.getElementById('contactForm');


// TODO: 1
contactForm.addEventListener("submit", function() {
    submitButton.disabled = true;
    setTimeout(function() {                                                                         
        submitButton.disabled = false;
    }, 5000);
})